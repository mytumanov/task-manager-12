package ru.mtumanov.tm.constant;

public class CommandConstant {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_COMMANDS = "commands";

    public static final String CMD_ARGUMENTS = "arguments";

    public static final String CMD_PROJECT_LIST = "project-list";

    public static final String CMD_PROJECT_CREATE = "project-create";

    public static final String CMD_PROJECT_CLEAR = "project-clear";

    public static final String CMD_PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String CMD_PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String CMD_PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public static final String CMD_PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public static final String CMD_PROJECT_START_BY_ID = "project-start-by-id";

    public static final String CMD_PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String CMD_PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public static final String CMD_PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    public static final String CMD_TASK_LIST = "task-list";

    public static final String CMD_TASK_CREATE = "task-create";

    public static final String CMD_TASK_CLEAR = "task-clear";

    public static final String CMD_TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String CMD_TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String CMD_TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public static final String CMD_TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public static final String CMD_TASK_START_BY_ID = "task-start-by-id";

    public static final String CMD_TASK_START_BY_INDEX = "task-start-by-index";

    public static final String CMD_TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public static final String CMD_TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

}
