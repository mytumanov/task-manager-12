package ru.mtumanov.tm.controller;

import ru.mtumanov.tm.api.controller.ICommandController;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.model.Command;
import ru.mtumanov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }
    
    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        System.out.println("Usage memory: " +  FormatUtil.formatBytes(usageMemory));
    }

    public void exit() {
        System.exit(0);
    }

    public void showArguemntError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
    }

    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
        System.exit(1);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Maksim Tumanov");
        System.out.println("e-mail: mytumanov@t1-consulting.ru");
        System.out.println("e-mail: MYTumanov@yandex.ru");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

    public void showCommands() {
        System.out.println("[Commands]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public void showArguments() {
        System.out.println("[Arguments]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }
    
}
