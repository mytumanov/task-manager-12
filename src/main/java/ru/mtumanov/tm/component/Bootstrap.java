package ru.mtumanov.tm.component;

import ru.mtumanov.tm.api.controller.ICommandController;
import ru.mtumanov.tm.api.controller.IProjectController;
import ru.mtumanov.tm.api.controller.ITaskController;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.controller.CommandController;
import ru.mtumanov.tm.controller.ProjectController;
import ru.mtumanov.tm.controller.TaskController;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.service.CommandService;
import ru.mtumanov.tm.service.ProjectService;
import ru.mtumanov.tm.service.TaskService;
import ru.mtumanov.tm.util.TerminalUtil;

public final class Bootstrap {
    
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        while (true) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.CMD_ABOUT:
                commandController.showAbout();
            break;
            case ArgumentConstant.CMD_HELP:
                commandController.showHelp();
            break;
            case ArgumentConstant.CMD_VERSION:
                commandController.showVersion();
            break;
            case ArgumentConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArguemntError();
            break;
        }
    }

    private void processCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConstant.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConstant.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConstant.CMD_EXIT:
                commandController.exit();
                break;
            case CommandConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
             case CommandConstant.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
             case CommandConstant.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConstant.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConstant.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConstant.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConstant.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConstant.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConstant.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConstant.CMD_TASK_SHOW_BY_INDEX:
                taskController.showTasktByIndex();
                break;
            case CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConstant.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConstant.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConstant.CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConstant.CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConstant.CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConstant.CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConstant.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConstant.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConstant.CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConstant.CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

}
