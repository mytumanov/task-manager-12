package ru.mtumanov.tm.api.controller;

public interface ITaskController {
    
    void createTask();

    void showTasks();

    void clearTasks();

    void showTaskById();

    void showTasktByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

}
