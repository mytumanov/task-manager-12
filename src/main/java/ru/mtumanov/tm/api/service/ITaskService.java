package ru.mtumanov.tm.api.service;

import java.util.List;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.model.Task;

public interface ITaskService {

    void add (Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();
    
    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
