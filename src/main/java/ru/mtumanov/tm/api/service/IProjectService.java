package ru.mtumanov.tm.api.service;

import java.util.List;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.model.Project;

public interface IProjectService {

    void add (Project project);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    void clear();
    
    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);
    
}
