package ru.mtumanov.tm.api.repository;

import java.util.List;

import ru.mtumanov.tm.model.Task;

public interface ITaskRepository {
    
    void add (Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

    void clear();

}
