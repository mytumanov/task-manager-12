package ru.mtumanov.tm.api.repository;

import java.util.List;

import ru.mtumanov.tm.model.Project;

public interface IProjectRepository {

    void add (Project project);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    void clear();
    
}
