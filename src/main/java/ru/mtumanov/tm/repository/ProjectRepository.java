package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.List;

import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.model.Project;

public final class ProjectRepository implements IProjectRepository {
    
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);        
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public int getSize() {
        return projects.size();
    }

}
