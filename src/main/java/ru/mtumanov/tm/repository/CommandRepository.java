package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {
    
    public static final Command CMD_HELP = new Command(CommandConstant.CMD_HELP, ArgumentConstant.CMD_HELP, "Show list arguments");

    public static final Command CMD_VERSION = new Command(CommandConstant.CMD_VERSION, ArgumentConstant.CMD_VERSION, "Show program version");

    public static final Command CMD_ABOUT = new Command(CommandConstant.CMD_ABOUT, ArgumentConstant.CMD_ABOUT, "Show about program");

    public static final Command CMD_INFO = new Command(CommandConstant.CMD_INFO, ArgumentConstant.CMD_INFO, "Show system info");

    public static final Command CMD_EXIT = new Command(CommandConstant.CMD_EXIT, null, "Close application");

    public static final Command CMD_COMMANDS = new Command(CommandConstant.CMD_COMMANDS, ArgumentConstant.CMD_COMMANDS, "Show command list");

    public static final Command CMD_ARGUMENTS = new Command(CommandConstant.CMD_ARGUMENTS, ArgumentConstant.CMD_ARGUMENTS, "Show argument list");

    public static final Command CMD_PROJECT_LIST = new Command(CommandConstant.CMD_PROJECT_LIST, null, "Show list projects");

    public static final Command CMD_PROJECT_CREATE = new Command(CommandConstant.CMD_PROJECT_CREATE, null, "Create new project");

    public static final Command CMD_PROJECT_CLEAR = new Command(CommandConstant.CMD_PROJECT_CLEAR, null, "Remove all projects");

    public static final Command CMD_PROJECT_SHOW_BY_ID = new Command(CommandConstant.CMD_PROJECT_SHOW_BY_ID, null, "Show project by id");

    public static final Command CMD_PROJECT_SHOW_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_SHOW_BY_INDEX, null, "Show project by index");

    public static final Command CMD_PROJECT_REMOVE_BY_ID = new Command(CommandConstant.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by id");

    public static final Command CMD_PROJECT_REMOVE_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index");

    public static final Command CMD_PROJECT_UPDATE_BY_ID = new Command(CommandConstant.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by id");

    public static final Command CMD_PROJECT_UPDATE_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index");

    public static final Command CMD_PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change project status by index");

    public static final Command CMD_PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandConstant.CMD_PROJECT_CHANGE_STATUS_BY_ID, null, "Change project status by id");

    public static final Command CMD_PROJECT_START_BY_ID = new Command(CommandConstant.CMD_PROJECT_START_BY_ID, null, "Start project by id");

    public static final Command CMD_PROJECT_START_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_START_BY_INDEX, null, "Start project by index");

    public static final Command CMD_PROJECT_COMPLETE_BY_ID = new Command(CommandConstant.CMD_PROJECT_COMPLETE_BY_ID, null, "Complete project by id");

    public static final Command CMD_PROJECT_COMPLETE_BY_INDEX = new Command(CommandConstant.CMD_PROJECT_COMPLETE_BY_INDEX, null, "Complete project by index");

    public static final Command CMD_TASK_LIST = new Command(CommandConstant.CMD_TASK_LIST, null, "Show list tasks");

    public static final Command CMD_TASK_CREATE = new Command(CommandConstant.CMD_TASK_CREATE, null, "Create new task");

    public static final Command CMD_TASK_CLEAR = new Command(CommandConstant.CMD_TASK_CLEAR, null, "Remove all tasks");

    public static final Command CMD_TASK_SHOW_BY_ID = new Command(CommandConstant.CMD_TASK_SHOW_BY_ID, null, "Show task by id");

    public static final Command CMD_TASK_SHOW_BY_INDEX = new Command(CommandConstant.CMD_TASK_SHOW_BY_INDEX, null, "Show task by index");

    public static final Command CMD_TASK_REMOVE_BY_ID = new Command(CommandConstant.CMD_TASK_REMOVE_BY_ID, null, "Remove task by id");

    public static final Command CMD_TASK_REMOVE_BY_INDEX = new Command(CommandConstant.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index");

    public static final Command CMD_TASK_UPDATE_BY_ID = new Command(CommandConstant.CMD_TASK_UPDATE_BY_ID, null, "Update task by id");

    public static final Command CMD_TASK_UPDATE_BY_INDEX = new Command(CommandConstant.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index");

    public static final Command CMD_TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandConstant.CMD_TASK_CHANGE_STATUS_BY_INDEX, null, "Change task status by index");

    public static final Command CMD_TASK_CHANGE_STATUS_BY_ID = new Command(CommandConstant.CMD_TASK_CHANGE_STATUS_BY_ID, null, "Change task status by id");

    public static final Command CMD_TASK_START_BY_ID = new Command(CommandConstant.CMD_TASK_START_BY_ID, null, "Start task by id");

    public static final Command CMD_TASK_START_BY_INDEX = new Command(CommandConstant.CMD_TASK_START_BY_INDEX, null, "Start task by index");

    public static final Command CMD_TASK_COMPLETE_BY_ID = new Command(CommandConstant.CMD_TASK_COMPLETE_BY_ID, null, "Complete task by id");

    public static final Command CMD_TASK_COMPLETE_BY_INDEX = new Command(CommandConstant.CMD_TASK_COMPLETE_BY_INDEX, null, "Complete task by index");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
        CMD_HELP, CMD_VERSION, CMD_ABOUT, CMD_INFO, CMD_COMMANDS, CMD_ARGUMENTS, 
        CMD_PROJECT_LIST, CMD_PROJECT_CREATE, CMD_PROJECT_CLEAR,
        CMD_PROJECT_SHOW_BY_ID, CMD_PROJECT_SHOW_BY_INDEX, CMD_PROJECT_REMOVE_BY_ID, CMD_PROJECT_REMOVE_BY_INDEX, CMD_PROJECT_UPDATE_BY_ID, CMD_PROJECT_UPDATE_BY_INDEX,
        CMD_PROJECT_CHANGE_STATUS_BY_INDEX, CMD_PROJECT_CHANGE_STATUS_BY_ID, CMD_PROJECT_START_BY_ID, CMD_PROJECT_START_BY_INDEX, CMD_PROJECT_COMPLETE_BY_ID, CMD_PROJECT_COMPLETE_BY_INDEX,
        CMD_TASK_LIST, CMD_TASK_CREATE, CMD_TASK_CLEAR,
        CMD_TASK_SHOW_BY_ID, CMD_TASK_SHOW_BY_INDEX, CMD_TASK_REMOVE_BY_ID, CMD_TASK_REMOVE_BY_INDEX, CMD_TASK_UPDATE_BY_ID, CMD_TASK_UPDATE_BY_INDEX,
        CMD_TASK_CHANGE_STATUS_BY_INDEX, CMD_TASK_CHANGE_STATUS_BY_ID, CMD_TASK_START_BY_ID, CMD_TASK_START_BY_INDEX, CMD_TASK_COMPLETE_BY_ID, CMD_TASK_COMPLETE_BY_INDEX,
        CMD_EXIT};

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
